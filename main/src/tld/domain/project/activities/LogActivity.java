package tld.domain.project.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import tld.domain.project.R;
import tld.domain.project.helpers.InternalLogHelper;

public class LogActivity extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

	String log = InternalLogHelper.getLog(this);
        TextView pid = (TextView)findViewById(R.id.text_log);
        pid.setText(log);
    }
}
