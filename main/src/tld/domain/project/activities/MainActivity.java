package tld.domain.project.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import tld.domain.project.R;
import tld.domain.project.helpers.InternalLogHelper;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        TextView pkg = (TextView)findViewById(R.id.text_package);
        pkg.setText(getPackageName());
        
        //Throw runtime exception
        Button button = (Button)findViewById(R.id.button_test);
        button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				throw new RuntimeException(getResources().getString(R.string.button_test));
			}
		});
        
        InternalLogHelper.writeLog(this, getResources().getString(R.string.app_started_message));
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Intent intent = null;
    	switch (item.getItemId()) {
			case R.id.item_intlog:
				intent = new Intent(MainActivity.this, LogActivity.class);
				break;
			case R.id.item_syslog:
				intent = new Intent(MainActivity.this, LogcatActivity.class);
				break;
			default:
				break;
		}
    	
    	if (intent != null) {
    		MainActivity.this.startActivity(intent);
    	}
    	
    	return true;
    }
}
