package tld.domain.project.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import tld.domain.project.R;
import tld.domain.project.helpers.InternalLogHelper;

public class CrashActivity extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash);

        Intent intent = getIntent();
        String log = intent.getStringExtra("crash_extra");
        TextView text = (TextView)findViewById(R.id.text_exception);
        text.setText(log);
        
        InternalLogHelper.writeLog(this, log);
        
        Button button = (Button)findViewById(R.id.button_terminate);
        button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finishAndRemoveTask();
				android.os.Process.killProcess(android.os.Process.myPid());
				System.exit(0);
			}
		});
    }
    
}
