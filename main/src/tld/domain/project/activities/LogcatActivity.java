package tld.domain.project.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import tld.domain.project.R;
import tld.domain.project.helpers.LogcatHelper;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class LogcatActivity extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logcat);
        
        Spinner dropdown = (Spinner)findViewById(R.id.spinner_level);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, LogcatHelper.priorities);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				RefreshLog(null);
			}
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				RefreshLog(position);
			}
		});
        
        //TODO: First text set to verify if scroll works
    	TextView text = (TextView)findViewById(R.id.text_logcat);
    	text.setText("");
        text.setMovementMethod(new ScrollingMovementMethod());
        text.append("");
    }
    
    private void RefreshLog(Integer position) {
    	String command = LogcatHelper.getCommand(position);
    	
    	TextView text = (TextView)findViewById(R.id.text_logcat);
    	text.setText("");
        text.setMovementMethod(new ScrollingMovementMethod());
        
        if(command != null) {
	        try {
	        	Process process = Runtime.getRuntime().exec(command);
	            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
	
	            String line = "";
	            while ((line = bufferedReader.readLine()) != null) {
	                text.append(System.lineSeparator());
	                text.append(line);
	            }
	        }
	        catch (IOException e) {
	        	text.append(getResources().getString(R.string.log_access_exception_message));
	        }
        }
    }
}
