package tld.domain.project;

import java.io.PrintWriter;
import java.io.StringWriter;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import tld.domain.project.activities.CrashActivity;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Thread.setDefaultUncaughtExceptionHandler(
				new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread thread, Throwable throwable) {

						//Get message
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						throwable.printStackTrace(pw);
						String message = throwable.getMessage() + System.lineSeparator() + sw.toString();
						
						//Open crash activity
						Context context = getApplicationContext();
						Intent intent = new Intent(context, CrashActivity.class);
						intent.putExtra("crash_extra", message);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						context.startActivity(intent);
						
						//Kill Process
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				});
	}
	
}
