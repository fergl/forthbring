package tld.domain.project.helpers;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import android.content.Context;
import android.util.Log;

public class InternalLogHelper {

public static String log = "log";

	public static boolean writeLog(Context context, String data) {
		boolean result = false;
		data = new Date() + System.lineSeparator() + data + System.lineSeparator();
        try (FileOutputStream fos = context.openFileOutput(log, Context.MODE_APPEND | Context.MODE_PRIVATE)){
            fos.write(data.getBytes());
            result = true;
        } catch (FileNotFoundException e) {
        	Log.e("InternalLogHelper", "File not found when attempting to write");
        } catch (IOException e) {
        	Log.e("InternalLogHelper", "File write failed when writting or closing");
        }
        return result;
    }

    public static String getLog(Context context) {
        String result = null;
        try (InputStream is = context.openFileInput(log)){
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream()){
                byte[] b = new byte[1024];
                int bytesRead = is.read(b);
                while (bytesRead != -1) {
                    bos.write(b, 0, bytesRead);
                    bytesRead = is.read(b);
                }
                result = bos.toString();
            } catch (IOException e) {
            	Log.e("InternalLogHelper", "File read failed when reading or closing output");
            }
        } catch (FileNotFoundException e) {
        	Log.e("InternalLogHelper", "File not found when attempting to read");
        } catch (IOException e) {
        	Log.e("InternalLogHelper", "File read failed when opening or closing input");
        }
        return result;
    }
}
