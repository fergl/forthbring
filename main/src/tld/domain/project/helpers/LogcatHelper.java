package tld.domain.project.helpers;

public class LogcatHelper {
	public static final String[] priorities = new String[] { "Verbose", "Debug", "Info", "Warn", "Error", "Fatal", "Silent" };
	
	public static String getCommand(Integer position) {
		String command = null;
		try {
			int index = Integer.valueOf(position);
			command = "logcat -d ";
		    	switch (index) {
		    		case 0:
		    			break;
				case 1:
					command+="*:D";
					break;
				case 2:
					command+="*:I";
					break;
				case 3:
					command+="*:W";
					break;
				case 4:
					command+="*:E";
					break;
				case 5:
					command+="*:F";
					break;
				case 6:
					command+="*:S";
					break;
				default:
					command = null;
					break;
				}
		}
		catch (NumberFormatException | NullPointerException e) {

		}
		return command;
	}
}
