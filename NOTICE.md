# SOURCE CODE

project forthbring™ is a software project originally created by Fernando Lamata in 2021, released under an MIT License and published on gitlab.com. Corresponding license can be found [here](./LICENSE).
This MIT License is limited only to the text source files of the project and does not license any image included in this repository or gives away any right corresponding to the project forthbring™ trademark (including its logo).

# IMAGES

Image "esheep default mipmap" (located in utils/image/esheep.png and also in main/res/mipmap/icon.png) is a creative work created by the author Fernando Lamata in 2021 and licensed under a Creative Commons Attribution-NonCommercial 4.0 International License. Corresponding license can be found [here](http://creativecommons.org/licenses/by-nc/4.0/).

The project forthbring™ logo (forthbring_logo.png) as creative work was designed by the author Fernando Lamata who holds and reserves all rights of copyright since its creation in 2021.

# TRADEMARK

The name project forthbring™ and its logo (forthbring_logo.png) constitute a trademark owned by Fernando Lamata. None of the licenses quoted in this document grant permission to use the project forthbring™ trademark (including its logo). Both name and logo are not intended and must not be used in any derivative work and shall not be used in any other software project. Nominative fair use of the project forthbring™ trademark is allowed when mentioning this work.
"project forthbring" can also be referred as software title, software name, project title and/or project name.
