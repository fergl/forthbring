# project forthbring™

![FC](./forthbring_logo.png)

The goal of this project is to provide a set-up to build your own applications for Android using a complete Open Source environment.
Of course, this is just an option among many others you can choose.
This readme file will describe the system installation, the IDE configuration and the build process for the apk (among other stuff).

## Current version
Current version is designed to work in a device that has no connection (both to internet or target device). At first, you will need to connect to internet to download all the software required to set up all the environment, but once it's working, you won't need to connect again (unless you plan to add external libraries or other stuff).
Makefile rules are used to build the apps.
Libraries need to be added manually.


## System Configuration

### Tools/Software involved
- Debian OS
- Debian's Android SDK (includes proguard)
- make
- Eclipse (which will be used in this example -but feel free to choose any other you prefer or build directly from command line-)
- jUnit
- OpenJDK

### Steps
- Install Debian OS (last tested with buster)
- Install OpenJDK (last tested with 11)
- Install Eclipse
- Install Debian's Android SDK (Detailed instructions can be found [here](https://wiki.debian.org/AndroidTools))*

*Key steps will be:
- apt-get install android-sdk-platform-tools
- apt-get install android-sdk
- apt-get install android-sdk-platform-23
- export ANDROID_HOME=/usr/lib/android-sdk


## Workspace Projects

Workspace consist in 3 projects:
- main (application files)
- utils (IDE helper files and build instructions)
- test (jUnit test files)


## Project/IDE Set-up

Instructions to set up the development environment

1. Download/clone all files.
2. Create a Workspace under project root folder
3. Add utils project
  - Create a new "Project" called "utils"
  - Add android-schema.dtd as SYSTEM http://android-schema.dtd in XML Catalog
4. Add main project
  - Create a new "Java project" named "main" (in this example with JDK 1.7) (remove module-info.java if suggested)
  - Add external library android.jar to classpath (probably in /usr/lib/android-sdk/platforms/android-23)
  - Add 3 External Tools (with common data: `Location: /bin/make`, `Working directory: main project` and `Refresh: Refresh resources upon completion`)
    - **"APK_build"** - Arguments: -f ../utils/Makefile build
    - **"APK_keyGenerator"** - Arguments: -f ../utils/Makefile generate-key
    - **"XML_Validation"** - Arguments: -f ../utils/Makefile r-generator
  - Execute "XML_Validation"
  - Import XML_Validator as project Builder with build options:
    - Allocate console
	- Run in background
	- After a "clean"
	- During manual builds
	- During Auto builds
	- Specify resources: res folder and AndroidManifest.xml
5. Add test project
  - Create new "Java project" called "test" with JDK 1.7
  - Add main project and jUnit4 to classpath

Last steps to provide your package name/config and finish project setup:
- refactor/rename root package (tld.domain.project)
- replace tld.domain.project in AndroidManifest.xml
- replace tld/domain/project in makefile
- generate key
- place your icon (res/mipmap/icon.png)
- change application name ("application_name" in res/values/strings.xml)

**Notes:**
- To add jars to be used as libraries: add them to libs folder and then to java build path in main project
- When creating new layout xml files, remember to add `<!DOCTYPE LinearLayout SYSTEM "http://android-schema.dtd">` like in the sample xml provided, so they will access android-schema.dtd for validation and suggestions
- If you want to add the sources and full javadoc from OpenJDK and Debian's Android SDK (recommended), you should also download those and set their location in Eclipse (for example /usr/lib/jvm/openjdk-11/src.zip, & android-framework-23_6.0.1+r72-5.orig)
- `xmlns:xsi="http://www.w3.org/2001/XMLSchema" xsi:noNamespaceSchemaLocation="../utils/schemas/manifest-schema.xsd"` in AndroidManifest.xml is included for xml validation (it is not necessary at all for your app, just for the development environment)


## How to test your app in an android environment?

Actually this template does not come with a way to debug the code in a runtime android environment.

So... What can you do instead?

This project provides some tools to test, log and gather valuable information from the runtime environment:
- LogcatActivity (displays the device log associated with the process)
- LogActivity (an internal separated log that you can use to test things in a cleaner way -recommended since many warnings for unmatching SDK resources will be written to logcat buffer during runtime-)
- CrashActivity (this replaces the "Application stopped working" message with the throwable stacktrace of the fatal error that crashed your app)
- test project (allowing you to test limited stuff with jUnit)

**Note:** remember to remove proguard from the build process, so the logs and messages won't display obfuscated info


## Next steps

- Add debug external tool
  - proguard must be removed from the build process (dx should get the class files from javac directly)
  - aapt package must have the debug flag
  - in App class remove the unhandledExceptions handler for regular build process
  - use debug signature for the apk
- Add MockContext support and sample test (so unit test can cover much more cases)
- Add test execution in makefile rules
- Add QEMU as emulator (hopefully with lineageOS or replicant)
- Offer gradle integration as option
- Automate all these steps
- Upgrade XML suggestions
- Update example with java 8


## Describing some of the work: build process

How the build process works (almost everything in makefile)
1. javac compiles everything inside the src/tld/domain/project folder recursively
2. proguard shrinks and obfuscates all the compiled files (optimize is disabled)
3. dx will dex all project compiled files (and also include the ones used from external libraries). This action optimizes the code and leaves it for Dalvik (nowdays ART). The result will all be contained in a single file "classes.dex".
4. appt will build an unsigned apk (gathering classes.dex, res files and the AndroidManifest.xml)
5. zipalign will align the apk
6. keytool will allow you to create a certificate if you haven't created it before
7. apksigner will sign the unsigned/aligned apk resulting in the final installable apk


## Describing some of the work: XSD & DTD provided

The XML supplied in this repository were built completely from the Debian's Android SDK.
It was made as a first version and remained that way so it has limited functionality.
There are lots of other options to supply suggestion and xml validations, but in this case, we will provide some notes of how these files were created, in case you are interested:

First, you need to find the Android SDK folder (probably in /usr/lib/android-sdk), where you will need android.jar and some txt files aside.

### Case 1: xml autocomplete for layouts (for example, an activity)

ATTRS
1. javap -cp android.jar -public  android.R.attr | grep "public static final int " > file
2. replace '  public static final int ' with ' android:'
3. replace ';' with ' CDATA #IMPLIED'
4. add ' xmlns:android CDATA #IMPLIED'
5. begin first line with "<!ENTITY % attrs '"
6. end last line with "'>"

ELEMENT
1. jar -tf android.jar | grep -E 'android/widget.*.class' > file
2. cat file | grep -v "\\$" > cfile
3. replace 'android/widget/' with '<!ELEMENT '
4. replace '.class' with ' ANY>'
5. duplicate file
6. replace '<!ELEMENT ' with '<!ATTLIST '
7. replace ' ANY>' with ' %attrs;>'


### Case 2: AndroidManifest autocomplete

This is the base skeleton and, next to some tags, which file included in the SDK provides useful entries to be used

```
<manifest>
	<uses-features android:name="*" /> features.txt
	<uses-permission android:name="*" /> api
	<application>
		<activity>
			<intent-filter>
				<action android:name="*" /> activity_actions.txt
				<category android:name="*" /> categories.txt
			</intent-filter>
		</activity>
	</application>
	<service android:permission="*"> api
		<intent-filter>
			<action android:name="*" /> service_actions.txt
		</intent-filter>
	</service>
	<receiver>
		<intent-filter>
			<action android:name="*" /> broadcast_actions.txt
		</intent-filter>
	</receiver>
</manifest>
```

**with txt files:**

replace '^android.' '<enumeration value="android.' + replace '\n' '"></enumeration>\n'

**for api entries:**
1. javap -cp android.jar -public  android.Manifest.permission | grep "public static final java-lang-String "
2. replace 'public static final java.lang.String ' '<enumeration value="android.permission.'
3. replace ';' '"></enumeration>'


**Note:** these files are mainly used for suggestion and IDE validation. The final check is performed by aapt itself and any error found will be shown in the Console. So the layout or AndroidManifest files will be properly revised, regardless of how you build these files or even if you don't include them as part of your development environment.


## Describing some of the work: proguard rules

The file rules.pro includes all rules used by ProGuard during the build process.
I read official documentation from guardsquare.com (full link below) and also revised ProGuard files included with the Debian's Android SDK in order to make it.
It is possible that you will need to add or modify current rules for your custom application, or even fix some of them.
They were just tested with this current project and a few other that kept the same base structure.


## Legal information

For licenses and legal information regarding the project forthbring™ trademark (including its logo: forthbring_logo.png), project forthbring™ source code and the utils/image/esheep.png "esheep default mipmap" image (also located in main/res/mipmap/icon.png), please refer to the [NOTICE file](./NOTICE.md).

Other company or product names mentioned in this file or in any other file in this repository, are or may be trademarks (registered or not) of their respective owners. This owners have not given any approval, sponsorship or endorsement and have no association or affiliation with project forthbring™.


## Recommended links

Recommended links and inspiration sources:
- Free your Android - A Free Software Foundation Europe activity (https://fsfe.org/activities/android/)
- Free phone operating system - A Free Software Foundation campaign (https://www.fsf.org/campaigns/priority-projects/free-phone)
- Android and Users' Freedom - A philosophy essays/article by Richard Stallman (https://www.gnu.org/philosophy/android-and-users-freedom.html)
- Replicant Web page (https://replicant.us)
- AndroidTools - android-tools packaging team "gathering place" (https://wiki.debian.org/AndroidTools)
- hello-world-debian-android - A "Hello World" app designed to be built only with tools included in Debian Buster by @Matrixcoffee (https://gitlab.com/Matrixcoffee/hello-world-debian-android)
- ProGuard manual (https://www.guardsquare.com/manual/home)
