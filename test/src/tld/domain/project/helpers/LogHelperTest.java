package tld.domain.project.helpers;

import org.junit.Assert;
import org.junit.Test;

public class LogHelperTest {
	@Test
	public void PrioritiesTest() {
		Assert.assertEquals(LogcatHelper.priorities.length, 7);
		Assert.assertEquals(LogcatHelper.priorities[0], "Verbose");
	}
	
	@Test
	public void GetCommandsTest() {
		Assert.assertEquals(LogcatHelper.getCommand(null), null);
		Assert.assertEquals(LogcatHelper.getCommand(-1), null);
		Assert.assertEquals(LogcatHelper.getCommand(9), null);
		Assert.assertEquals(LogcatHelper.getCommand(2), "logcat -d *:I");
	}
}
